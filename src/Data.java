
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yo
 */
public class Data implements Serializable {

    private int value;

    public Data(int inti) {
        value = inti;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Data && ((Data) obj).value == value) {
            return true;
        }
        return false;
    }
    
    public void setValue(int val){
        value=val;
    }
    public int getValue(){
    return value;
    }
}  
   


