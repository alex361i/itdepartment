
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yo
 */
public class Deserializer {

    public static void main(String[] args) {
        Deserializer deserialize = new Deserializer();
        Issue issue = deserialize.deserializeIssue();
        System.out.println(issue);
    }

    public Issue deserializeIssue() {
        Issue issue;

        try {
            FileInputStream fin = new FileInputStream("database.txt");
            ObjectInputStream ois = new ObjectInputStream(fin);
            issue = (Issue) ois.readObject();
            ois.close();}
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        
    }   

}
