import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yo
 */
public class Issue implements Serializable{
    
     
    private  String Subject;
    private  String priority;
    private  String status;
    private  String comment;
    
    public Issue() {
        this(" ","","","");
    }
    public Issue(String Subject, String priority, String status,String comment) {
        this.Subject = Subject;
        this.priority = priority;
        this.status = status;
        this.comment=comment;
        

    }

    public static void main(String[] args) throws FileNotFoundException {
      /*  ArrayList<Issue> itIssue = new ArrayList<Issue>();
        //for(int i=0;i<itIssue;i++)
        itIssue.add( new Issue("Computer Locked",20,10,"Hello.Please save this in the mentioned file."));
       ArrayList<String> arrl = new ArrayList<String>();
        //adding elements to the end
        arrl.add("First");
        arrl.add("Second");
        arrl.add("Third");
        arrl.add("Random");
        Iterator<String> itr = arrl.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
    }*/
 } 
    
    public String getSubject() {
        return Subject;
    }
 
    public void setSubject(String Subject) {
        this.Subject=Subject;
    }

    public String SubjectProperty() {
        return Subject;
    }
 
    public String getPriority() {
        return priority;
    }
    
    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int PriorityProperty(int Priority) {
        return Priority;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public int StatusProperty(int Status) {
        return Status;
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public String CommentProperty(String comment) {
        return comment;
    }
}
