
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author yo
 */
public class MainClassData {

    public static void main(String[] args) {
        Data data = new Data(1);
        try {
            ObjectOutputStream objectOut = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("database.txt")));
            objectOut.writeObject(data);
            System.out.println("first object writen has value " + data.getValue());
            data.setValue(2);
            objectOut.writeObject(data);
            System.out.println("2nd object writen has value :" + data.getValue());
            data.setValue(3);
            objectOut.writeObject(data);
            System.out.println("3rd object writen has value :"+ data.getValue());
            objectOut.close();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        try {
            ObjectInputStream objectIn = new ObjectInputStream(new BufferedInputStream(new FileInputStream("database.txt")));
            Data data1=(Data) objectIn.readObject();
            Data data2=(Data) objectIn.readObject();
            Data data3=(Data) objectIn.readObject();
            
            System.out.println(data1.equals(data2));
            System.out.println(data2.equals(data3));
            System.out.println(data1.getValue());
            System.out.println(data2.getValue());
            System.out.println(data3.getValue());
            objectIn.close();
        
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

}
